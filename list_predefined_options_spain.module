<?php

/**
 * @file
 * Code for the list_predefined_options_spain module.
 */

/**
 * Implements hook_list_option_info().
 */
function list_predefined_options_spain_list_option_info() {
  $info['es_provinces'] = array(
    'label' => t('Spanish Provinces'),
    'callback' => 'list_predefined_options_spain_list_option_es_provinces',
  );
  $info['es_ccaa'] = array(
    'label' => t('Spanish States (Comunidad Autónoma)'),
    'callback' => 'list_predefined_options_spain_list_option_es_ccaa',
  );

  return $info;
}

/**
 * List option callback for Spanish Provinces.
 *
 * @return array()
 *   An array of Spanish provinces, suitable for being used in #options lists.
 */
function list_predefined_options_spain_list_option_es_provinces() {
  // Note the names are not translated on purpose. Use the alter hook to modify
  // them if you need.
  $options['ES-C'] = 'La Coruña';
  $options['ES-VI'] = 'Álava';
  $options['ES-AB'] = 'Albacete';
  $options['ES-A'] = 'Alicante';
  $options['ES-AL'] = 'Almería';
  $options['ES-O'] = 'Asturias';
  $options['ES-AV'] = 'Ávila';
  $options['ES-BA'] = 'Badajoz';
  $options['ES-PM'] = 'Baleares';
  $options['ES-B'] = 'Barcelona';
  $options['ES-BU'] = 'Burgos';
  $options['ES-CC'] = 'Cáceres';
  $options['ES-CA'] = 'Cádiz';
  $options['ES-S'] = 'Cantabria';
  $options['ES-CS'] = 'Castellón';
  $options['ES-CR'] = 'Ciudad Real';
  $options['ES-CO'] = 'Córdoba';
  $options['ES-CU'] = 'Cuenca';
  $options['ES-GI'] = 'Gerona';
  $options['ES-GR'] = 'Granada';
  $options['ES-GU'] = 'Guadalajara';
  $options['ES-SS'] = 'Gipuzkoa';
  $options['ES-H'] = 'Huelva';
  $options['ES-HU'] = 'Huesca';
  $options['ES-J'] = 'Jaén';
  $options['ES-LO'] = 'La Rioja';
  $options['ES-GC'] = 'Las Palmas';
  $options['ES-LE'] = 'León';
  $options['ES-L'] = 'Lérida';
  $options['ES-LU'] = 'Lugo';
  $options['ES-M'] = 'Madrid';
  $options['ES-MA'] = 'Málaga';
  $options['ES-MU'] = 'Murcia';
  $options['ES-NA'] = 'Navarra';
  $options['ES-OR'] = 'Orense';
  $options['ES-P'] = 'Palencia';
  $options['ES-PO'] = 'Pontevedra';
  $options['ES-SA'] = 'Salamanca';
  $options['ES-TF'] = 'Santa Cruz de Tenerife';
  $options['ES-SG'] = 'Segovia';
  $options['ES-SE'] = 'Sevilla';
  $options['ES-SO'] = 'Soria';
  $options['ES-T'] = 'Tarragona';
  $options['ES-TE'] = 'Teruel';
  $options['ES-TO'] = 'Toledo';
  $options['ES-V'] = 'Valencia';
  $options['ES-VA'] = 'Valladolid';
  $options['ES-BI'] = 'Bizkaia';
  $options['ES-ZA'] = 'Zamora';
  $options['ES-Z'] = 'Zaragoza';
  drupal_alter('list_predefined_options_spain_provinces', $options);
  return $options;
}

/**
 * List option callback for Spanish States (Comunidad Autónoma).
 *
 * @return array()
 *   An array of Spanish states, suitable for being used in #options lists.
 */
function list_predefined_options_spain_list_option_es_ccaa() {
  // Note the names are not translated on purpose. Use the alter hook to modify
  // them if you need.
  $options['ES-AN'] = 'Andalucía';
  $options['ES-AR'] = 'Aragón';
  $options['ES-AS'] = 'Asturias, Principado de';
  $options['ES-CN'] = 'Canarias';
  $options['ES-CB'] = 'Cantabria';
  $options['ES-CM'] = 'Castilla-La Mancha';
  $options['ES-CL'] = 'Castilla y León';
  $options['ES-CT'] = 'Cataluña';
  $options['ES-EX'] = 'Extremadura';
  $options['ES-GA'] = 'Galicia';
  $options['ES-IB'] = 'Islas Baleares';
  $options['ES-RI'] = 'La Rioja';
  $options['ES-MD'] = 'Madrid, Comunidad de';
  $options['ES-MC'] = 'Murcia, Región de';
  $options['ES-NC'] = 'Navarra, Comunidad Foral de';
  $options['ES-PV'] = 'País Vasco';
  $options['ES-VC'] = 'Valenciana, Comunidad';
  $options['ES-CE'] = 'Ceuta';
  $options['ES-ML'] = 'Melilla';
  drupal_alter('list_predefined_options_spain_ccaa', $options);
  return $options;
}
